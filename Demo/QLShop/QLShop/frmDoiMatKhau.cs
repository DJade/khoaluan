﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DBReference;
namespace QLShop
{
    public partial class frmDoiMatKhau : DevExpress.XtraEditors.XtraForm
    {
        private string errorMess;

        public frmDoiMatKhau()
        {
            InitializeComponent();
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            string query = string.Format(@"EXEC sp_doi_mat_khau '{0}', '{1}', '{2}'",txtID.Text,txtCu.Text,txtMoi.Text);
            errorMess = DataUtilityReader.GetScalarValue(DataUtilityReader.connStr_DB, query).ToString();
            if(!string.IsNullOrEmpty(errorMess))
            {
                lblError.Visible = true;
                lblError.Text = errorMess;
                return;
            }
            MessageBox.Show("Đổi mật khẩu thành công!!");
        }

        private void txtID_Leave(object sender, EventArgs e)
        {
            TextEdit control = (TextEdit)sender;
            errorMess = CheckEmpty(control.Text);
            if (!string.IsNullOrEmpty(errorMess))
            {
                lblError.Visible = true;
                lblError.Text = errorMess;
                control.Focus();
            }
        }

        private string CheckEmpty(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return "Giá trị không được để trống!!";
            }

            if(!string.IsNullOrEmpty(txtMoi.Text))
            {
                if(!value.Equals(txtMoi.Text))
                {
                    return "Mật khẩu mới không giống nhau!!";
                }
            }

            return null;
        }

        private void txtID_EditValueChanged(object sender, EventArgs e)
        {
            lblError.Visible = false;
        }
    }
}