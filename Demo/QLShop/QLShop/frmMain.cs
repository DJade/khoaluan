﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DBReference;
using System.Configuration;
namespace QLShop
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Properties
        public string tenNhanVien = string.Empty;
        public static bool isShowPhanQuyen;
        public static bool isShowQLNV;
        public static bool isShowDanhMuc;
        public static bool isShowGia;
        public static bool isShowKho;
        public static bool isShowNCC;
        public static bool isShowKhachHang;
        public static bool isShowTongQuan;
        public static bool isShowTimSP;
        public static bool isShowTimNV;
        public static bool isShowThongTin;

        private frmPhanQuyen phanQuyen;
        private frmQuanLyNhanVien nv;
        private frmSanPham sp;
        private frmGia gia;
        private frmKho kho;
        private frmNCC ncc;
        private frmKhachHang kh;
        private frmTongQuan tq;
        private frmTimSanPham tsp;
        private frmTimNV tnv;
        private frmThongTin tt;
        #endregion

        #region Main
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {            
            //string masterConnStr = ConfigurationManager.ConnectionStrings["MasterDBConnectionString"].ToString();
            //string dbConnStr = DataUtilityReader.GetScalarValue(masterConnStr, "SELECT VALUE FROM CONFIGURATION WHERE NAME = 'ConnStr_DB'").ToString();
            //DataUtilityReader.InitConnection(masterConnStr, dbConnStr);
            Time.Start();
            //lblChaoMung.Caption = string.Format("Nhân Viên: {0} ",tenNhanVien);
        }
        #endregion

        #region Methods
        private void btnBackup_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmBackUp backup = new frmBackUp();
            backup.ShowDialog();
        }

        private void btnRestore_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmRestore restore = new frmRestore();
            restore.ShowDialog();
        }

        private void btnDangXuat_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void btnDoiMatKhau_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmDoiMatKhau dmk = new frmDoiMatKhau();
            dmk.ShowDialog();
        }

        private void btnPhanQuyen_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowPhanQuyen)
            {
                phanQuyen = new frmPhanQuyen();
                phanQuyen.MdiParent = this;
                isShowPhanQuyen = true;
                phanQuyen.Show();
            }
            else
            {
                phanQuyen.Activate();
            }
        }

        private void btnQuanLyNV_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowQLNV)
            {
                nv = new frmQuanLyNhanVien();
                nv.MdiParent = this;
                isShowQLNV = true;
                nv.Show();
            }
            else
            {
                nv.Activate();
            }
        }

        private void btnDanhMuc_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowDanhMuc)
            {
                sp = new frmSanPham();
                sp.MdiParent = this;
                isShowDanhMuc = true;
                sp.Show();
            }
            else
            {
                sp.Activate();
            }
        }

        private void btnThietLapGia_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowGia)
            {
                gia = new frmGia();
                gia.MdiParent = this;
                isShowGia = true;
                gia.Show();
            }
            else
            {
                gia.Activate();
            }
        }

        private void btnKiemKho_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowKho)
            {
                kho = new frmKho();
                kho.MdiParent = this;
                isShowKho = true;
                kho.Show();
            }
            else
            {
                kho.Activate();
            }
        }

        private void btnNcc_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowNCC)
            {
                ncc = new frmNCC();
                ncc.MdiParent = this;
                isShowNCC = true;
                ncc.Show();
            }
            else
            {
                ncc.Activate();
            }
        }

        private void btnKhachHang_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowKhachHang)
            {
                kh = new frmKhachHang();
                kh.MdiParent = this;
                isShowKhachHang = true;
                kh.Show();
            }
            else
            {
                kh.Activate();
            }
        }

        private void btnTongQuan_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowTongQuan)
            {
                tq = new frmTongQuan();
                tq.MdiParent = this;
                isShowTongQuan = true;
                tq.Show();
            }
            else
            {
                tq.Activate();
            }
        }

        private void btnTimSP_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowTimSP)
            {
                tsp = new frmTimSanPham();
                tsp.MdiParent = this;
                isShowTimSP = true;
                tsp.Show();
            }
            else
            {
                tsp.Activate();
            }
        }

        private void btnTimNV_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowTimNV)
            {
                tnv = new frmTimNV();
                tnv.MdiParent = this;
                isShowTimNV = true;
                tnv.Show();
            }
            else
            {
                tnv.Activate();
            }
        }

        private void btnThongTin_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!isShowThongTin)
            {
                tt = new frmThongTin();
                tt.MdiParent = this;
                isShowThongTin = true;
                tt.Show();
            }
            else
            {
                tt.Activate();
            }
        }

        private void Time_Tick(object sender, EventArgs e)
        {
            lblShowTime.Caption = DateTime.Now.ToLocalTime().ToString("HH:mm:ss");
        }
        #endregion


    }
}