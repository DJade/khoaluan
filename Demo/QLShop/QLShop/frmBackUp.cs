﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DBReference;

namespace QLShop
{
    public partial class frmBackUp : DevExpress.XtraEditors.XtraForm
    {
        private string errorMess;

        public frmBackUp()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.ShowDialog();
            string filePath = folderBrowserDialog.SelectedPath;
            txtFilePath.Text = filePath;
        }

        private void btnBackUp_Click(object sender, EventArgs e)
        {
            picLoading.Visible = true;
            picLoading.Dock = DockStyle.Fill;
            backgroundWorker.RunWorkerAsync();
        }

        private void frmBackUp_Load(object sender, EventArgs e)
        {
            DataSet data = DataUtilityReader.GetData(DataUtilityReader.connStr_Master, "SELECT NAME FROM sys.databases");
            cboDB.Properties.Items.AddRange(data.Tables[0].Rows);
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string query = string.Format(@" BACKUP DATABASE {0} TO DISK='{1}\{2}.bak'   
                                            WITH   
                                               FORMAT,   
                                               COMPRESSION;", cboDB.SelectedText, txtFilePath.Text, txtFileName.Text);
                DataUtilityReader.GetData(DataUtilityReader.connStr_DB, query);
                errorMess = null;
            }
            catch(Exception ex)
            {
                errorMess = ex.Message;
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (string.IsNullOrEmpty(errorMess))
            {
                MessageBox.Show("Backup data thành công!!");
                picLoading.Visible = false;
                this.Close();
            }
            else
            {
                MessageBox.Show(string.Format("Backup data thất bại!!\n Error Message:{0}",errorMess));
                picLoading.Visible = false;
            }
        }
    }
}