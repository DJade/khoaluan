﻿namespace QLShop
{
    partial class frmPhanQuyen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboChucVu = new DevExpress.XtraEditors.LookUpEdit();
            this.dgvManHinhKoQuyen = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dgvManHinhCoQuyen = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.picAdd = new System.Windows.Forms.PictureBox();
            this.picDelete = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.cboChucVu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvManHinhKoQuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvManHinhCoQuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // cboChucVu
            // 
            this.cboChucVu.Location = new System.Drawing.Point(27, 22);
            this.cboChucVu.Name = "cboChucVu";
            this.cboChucVu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboChucVu.Properties.Appearance.Options.UseFont = true;
            this.cboChucVu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboChucVu.Size = new System.Drawing.Size(770, 26);
            this.cboChucVu.TabIndex = 1;
            this.cboChucVu.EditValueChanged += new System.EventHandler(this.cboChucVu_EditValueChanged);
            // 
            // dgvManHinhKoQuyen
            // 
            this.dgvManHinhKoQuyen.Location = new System.Drawing.Point(27, 63);
            this.dgvManHinhKoQuyen.MainView = this.gridView1;
            this.dgvManHinhKoQuyen.Name = "dgvManHinhKoQuyen";
            this.dgvManHinhKoQuyen.Size = new System.Drawing.Size(349, 364);
            this.dgvManHinhKoQuyen.TabIndex = 2;
            this.dgvManHinhKoQuyen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.dgvManHinhKoQuyen;
            this.gridView1.Name = "gridView1";
            // 
            // dgvManHinhCoQuyen
            // 
            this.dgvManHinhCoQuyen.Location = new System.Drawing.Point(442, 63);
            this.dgvManHinhCoQuyen.MainView = this.gridView2;
            this.dgvManHinhCoQuyen.Name = "dgvManHinhCoQuyen";
            this.dgvManHinhCoQuyen.Size = new System.Drawing.Size(355, 364);
            this.dgvManHinhCoQuyen.TabIndex = 3;
            this.dgvManHinhCoQuyen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.dgvManHinhCoQuyen;
            this.gridView2.Name = "gridView2";
            // 
            // picAdd
            // 
            this.picAdd.Image = global::QLShop.Properties.Resources.add_d6P_icon;
            this.picAdd.Location = new System.Drawing.Point(377, 174);
            this.picAdd.Name = "picAdd";
            this.picAdd.Size = new System.Drawing.Size(64, 64);
            this.picAdd.TabIndex = 4;
            this.picAdd.TabStop = false;
            this.picAdd.Click += new System.EventHandler(this.picAdd_Click);
            // 
            // picDelete
            // 
            this.picDelete.BackColor = System.Drawing.Color.Transparent;
            this.picDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picDelete.Image = global::QLShop.Properties.Resources.return_AV4_icon;
            this.picDelete.ImageLocation = "";
            this.picDelete.Location = new System.Drawing.Point(377, 269);
            this.picDelete.Name = "picDelete";
            this.picDelete.Size = new System.Drawing.Size(64, 64);
            this.picDelete.TabIndex = 5;
            this.picDelete.TabStop = false;
            this.picDelete.Click += new System.EventHandler(this.picDelete_Click);
            // 
            // frmPhanQuyen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 449);
            this.Controls.Add(this.picDelete);
            this.Controls.Add(this.picAdd);
            this.Controls.Add(this.dgvManHinhCoQuyen);
            this.Controls.Add(this.dgvManHinhKoQuyen);
            this.Controls.Add(this.cboChucVu);
            this.Name = "frmPhanQuyen";
            this.Text = "frmPhanQuyen";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPhanQuyen_FormClosed);
            this.Load += new System.EventHandler(this.frmPhanQuyen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cboChucVu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvManHinhKoQuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvManHinhCoQuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDelete)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit cboChucVu;
        private DevExpress.XtraGrid.GridControl dgvManHinhKoQuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl dgvManHinhCoQuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.PictureBox picAdd;
        private System.Windows.Forms.PictureBox picDelete;
    }
}