﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DBReference;
namespace QLShop
{
    public partial class frmPhanQuyen : DevExpress.XtraEditors.XtraForm
    {
        private string query;
        public frmPhanQuyen()
        {
            InitializeComponent();
        }

        private void frmPhanQuyen_Load(object sender, EventArgs e)
        {
            string query = string.Format("EXEC sp_get_chuc_vu");
            DataSet data = DataUtilityReader.GetData(DataUtilityReader.connStr_DB, query);
            cboChucVu.Properties.DataSource = data.Tables[0];
            cboChucVu.Properties.DisplayMember="TEN_CHUC_VU";
            cboChucVu.Properties.ValueMember="MA_CHUC_VU";
        }

        private void frmPhanQuyen_FormClosed(object sender, FormClosedEventArgs e)
        {
            frmMain.isShowPhanQuyen = false;
        }

        private void picAdd_Click(object sender, EventArgs e)
        {
            query = string.Format("EXEC sp_add_phan_quyen {0}", cboChucVu.EditValue.ToString());
            DataUtilityReader.GetData(DataUtilityReader.connStr_DB, query);
        }

        private void picDelete_Click(object sender, EventArgs e)
        {
            query = string.Format("EXEC sp_delete_phan_quyen {0}", cboChucVu.EditValue.ToString());
            DataUtilityReader.GetData(DataUtilityReader.connStr_DB, query);
        }

        private void cboChucVu_EditValueChanged(object sender, EventArgs e)
        {
            query = string.Format("EXEC sp_get_data_dgv_phan_quyen {0}",cboChucVu.EditValue.ToString());
            DataSet data = DataUtilityReader.GetData(DataUtilityReader.connStr_DB, query);
            dgvManHinhKoQuyen.DataSource = data.Tables[0];
            dgvManHinhCoQuyen.DataSource = data.Tables[1];
        }
    }
}