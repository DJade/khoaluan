﻿namespace QLShop
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraBars.Ribbon.ReduceOperation reduceOperation1 = new DevExpress.XtraBars.Ribbon.ReduceOperation();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnDangXuat = new DevExpress.XtraBars.BarButtonItem();
            this.btnDoiMatKhau = new DevExpress.XtraBars.BarButtonItem();
            this.btnBackup = new DevExpress.XtraBars.BarButtonItem();
            this.btnRestore = new DevExpress.XtraBars.BarButtonItem();
            this.btnPhanQuyen = new DevExpress.XtraBars.BarButtonItem();
            this.btnQuanLyNV = new DevExpress.XtraBars.BarButtonItem();
            this.lblChaoMung = new DevExpress.XtraBars.BarStaticItem();
            this.lblTime = new DevExpress.XtraBars.BarStaticItem();
            this.cboChiNhanh = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.btnDanhMuc = new DevExpress.XtraBars.BarButtonItem();
            this.btnThietLapGia = new DevExpress.XtraBars.BarButtonItem();
            this.btnKiemKho = new DevExpress.XtraBars.BarButtonItem();
            this.btnNcc = new DevExpress.XtraBars.BarButtonItem();
            this.btnKhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnTongQuan = new DevExpress.XtraBars.BarButtonItem();
            this.btnTimSP = new DevExpress.XtraBars.BarButtonItem();
            this.btnTimNV = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongTin = new DevExpress.XtraBars.BarButtonItem();
            this.lblShowTime = new DevExpress.XtraBars.BarStaticItem();
            this.pageHethong = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.grpgrpTaiKhoan = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.grpSaoLuu = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.grpQuanLy = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageSanPham = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.grpQuanLySanPham = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageGiaoDich = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.grpGiaoDich = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.pageTroGiup = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.grpTroGiup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.MainManager = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.Time = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnDangXuat,
            this.btnDoiMatKhau,
            this.btnBackup,
            this.btnRestore,
            this.btnPhanQuyen,
            this.btnQuanLyNV,
            this.lblChaoMung,
            this.lblTime,
            this.cboChiNhanh,
            this.btnDanhMuc,
            this.btnThietLapGia,
            this.btnKiemKho,
            this.btnNcc,
            this.btnKhachHang,
            this.btnTongQuan,
            this.btnTimSP,
            this.btnTimNV,
            this.btnThongTin,
            this.lblShowTime});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 1;
            this.ribbon.Name = "ribbon";
            this.ribbon.PageHeaderItemLinks.Add(this.lblChaoMung);
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.pageHethong,
            this.pageSanPham,
            this.pageGiaoDich,
            this.pageTroGiup});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemTimeEdit1});
            this.ribbon.Size = new System.Drawing.Size(935, 162);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.Caption = "Đăng Xuất";
            this.btnDangXuat.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnDangXuat.Id = 1;
            this.btnDangXuat.LargeGlyph = global::QLShop.Properties.Resources.sign_out;
            this.btnDangXuat.LargeWidth = 80;
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btnDangXuat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDangXuat_ItemClick);
            // 
            // btnDoiMatKhau
            // 
            this.btnDoiMatKhau.Caption = "Đổi Mật Khẩu";
            this.btnDoiMatKhau.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnDoiMatKhau.Id = 2;
            this.btnDoiMatKhau.LargeGlyph = global::QLShop.Properties.Resources.change_pass;
            this.btnDoiMatKhau.LargeWidth = 80;
            this.btnDoiMatKhau.Name = "btnDoiMatKhau";
            this.btnDoiMatKhau.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnDoiMatKhau.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDoiMatKhau_ItemClick);
            // 
            // btnBackup
            // 
            this.btnBackup.Caption = "Sao Lưu";
            this.btnBackup.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnBackup.Id = 3;
            this.btnBackup.LargeGlyph = global::QLShop.Properties.Resources.restore;
            this.btnBackup.LargeWidth = 80;
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBackup_ItemClick);
            // 
            // btnRestore
            // 
            this.btnRestore.Caption = "Phục Hồi";
            this.btnRestore.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnRestore.Id = 4;
            this.btnRestore.LargeGlyph = global::QLShop.Properties.Resources.backup;
            this.btnRestore.LargeWidth = 80;
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRestore_ItemClick);
            // 
            // btnPhanQuyen
            // 
            this.btnPhanQuyen.Caption = "Phân Quyền";
            this.btnPhanQuyen.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnPhanQuyen.Id = 5;
            this.btnPhanQuyen.LargeGlyph = global::QLShop.Properties.Resources.right1;
            this.btnPhanQuyen.LargeWidth = 80;
            this.btnPhanQuyen.Name = "btnPhanQuyen";
            this.btnPhanQuyen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPhanQuyen_ItemClick);
            // 
            // btnQuanLyNV
            // 
            this.btnQuanLyNV.Caption = "Quản Lý Nhân Viên";
            this.btnQuanLyNV.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnQuanLyNV.Id = 6;
            this.btnQuanLyNV.LargeGlyph = global::QLShop.Properties.Resources.user_edit1;
            this.btnQuanLyNV.LargeWidth = 80;
            this.btnQuanLyNV.Name = "btnQuanLyNV";
            this.btnQuanLyNV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnQuanLyNV_ItemClick);
            // 
            // lblChaoMung
            // 
            this.lblChaoMung.Caption = "Chào mừng";
            this.lblChaoMung.Id = 9;
            this.lblChaoMung.Name = "lblChaoMung";
            this.lblChaoMung.TextAlignment = System.Drawing.StringAlignment.Near;
            this.lblChaoMung.Width = 200;
            // 
            // lblTime
            // 
            this.lblTime.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lblTime.Caption = "Thời gian:";
            this.lblTime.Id = 10;
            this.lblTime.Name = "lblTime";
            this.lblTime.TextAlignment = System.Drawing.StringAlignment.Near;
            this.lblTime.Width = 60;
            // 
            // cboChiNhanh
            // 
            this.cboChiNhanh.Caption = "Chi Nhánh:";
            this.cboChiNhanh.Edit = this.repositoryItemComboBox1;
            this.cboChiNhanh.Id = 1;
            this.cboChiNhanh.Name = "cboChiNhanh";
            this.cboChiNhanh.Width = 250;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // btnDanhMuc
            // 
            this.btnDanhMuc.Caption = "Danh Mục Sản Phẩm";
            this.btnDanhMuc.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnDanhMuc.Id = 2;
            this.btnDanhMuc.LargeGlyph = global::QLShop.Properties.Resources.store;
            this.btnDanhMuc.LargeWidth = 80;
            this.btnDanhMuc.Name = "btnDanhMuc";
            this.btnDanhMuc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDanhMuc_ItemClick);
            // 
            // btnThietLapGia
            // 
            this.btnThietLapGia.Caption = "Thiết Lập Giá";
            this.btnThietLapGia.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnThietLapGia.Id = 3;
            this.btnThietLapGia.LargeGlyph = global::QLShop.Properties.Resources.price;
            this.btnThietLapGia.LargeWidth = 80;
            this.btnThietLapGia.Name = "btnThietLapGia";
            this.btnThietLapGia.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThietLapGia_ItemClick);
            // 
            // btnKiemKho
            // 
            this.btnKiemKho.Caption = "Kiểm Kho";
            this.btnKiemKho.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnKiemKho.Id = 4;
            this.btnKiemKho.LargeGlyph = global::QLShop.Properties.Resources.clipboard;
            this.btnKiemKho.LargeWidth = 80;
            this.btnKiemKho.Name = "btnKiemKho";
            this.btnKiemKho.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKiemKho_ItemClick);
            // 
            // btnNcc
            // 
            this.btnNcc.Caption = "Nhà Cung Cấp";
            this.btnNcc.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnNcc.Id = 5;
            this.btnNcc.LargeGlyph = global::QLShop.Properties.Resources.provider;
            this.btnNcc.LargeWidth = 80;
            this.btnNcc.Name = "btnNcc";
            this.btnNcc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNcc_ItemClick);
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.Caption = "Khách Hàng";
            this.btnKhachHang.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnKhachHang.Id = 6;
            this.btnKhachHang.LargeGlyph = global::QLShop.Properties.Resources.customer;
            this.btnKhachHang.LargeWidth = 80;
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhachHang_ItemClick);
            // 
            // btnTongQuan
            // 
            this.btnTongQuan.Caption = "Tổng Quan";
            this.btnTongQuan.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnTongQuan.Id = 7;
            this.btnTongQuan.LargeGlyph = global::QLShop.Properties.Resources.overview;
            this.btnTongQuan.LargeWidth = 80;
            this.btnTongQuan.Name = "btnTongQuan";
            this.btnTongQuan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTongQuan_ItemClick);
            // 
            // btnTimSP
            // 
            this.btnTimSP.Caption = "Tìm Sản Phẩm";
            this.btnTimSP.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnTimSP.Id = 8;
            this.btnTimSP.LargeGlyph = global::QLShop.Properties.Resources.search_item;
            this.btnTimSP.LargeWidth = 80;
            this.btnTimSP.Name = "btnTimSP";
            this.btnTimSP.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTimSP_ItemClick);
            // 
            // btnTimNV
            // 
            this.btnTimNV.Caption = "Tìm Nhân Viên";
            this.btnTimNV.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnTimNV.Id = 9;
            this.btnTimNV.LargeGlyph = global::QLShop.Properties.Resources.search_employee;
            this.btnTimNV.LargeWidth = 80;
            this.btnTimNV.Name = "btnTimNV";
            this.btnTimNV.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTimNV_ItemClick);
            // 
            // btnThongTin
            // 
            this.btnThongTin.Caption = "Thông Tin Ứng Dụng";
            this.btnThongTin.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.btnThongTin.Id = 10;
            this.btnThongTin.LargeGlyph = global::QLShop.Properties.Resources.info;
            this.btnThongTin.LargeWidth = 80;
            this.btnThongTin.Name = "btnThongTin";
            this.btnThongTin.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongTin_ItemClick);
            // 
            // lblShowTime
            // 
            this.lblShowTime.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lblShowTime.Caption = "barStaticItem1";
            this.lblShowTime.Id = 12;
            this.lblShowTime.Name = "lblShowTime";
            this.lblShowTime.TextAlignment = System.Drawing.StringAlignment.Near;
            this.lblShowTime.Width = 90;
            // 
            // pageHethong
            // 
            this.pageHethong.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageHethong.Appearance.Options.UseFont = true;
            this.pageHethong.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.grpgrpTaiKhoan,
            this.grpSaoLuu,
            this.grpQuanLy});
            this.pageHethong.Image = global::QLShop.Properties.Resources.system_730_icon;
            this.pageHethong.Name = "pageHethong";
            reduceOperation1.Behavior = DevExpress.XtraBars.Ribbon.ReduceOperationBehavior.Single;
            reduceOperation1.Group = null;
            reduceOperation1.ItemLinkIndex = 0;
            reduceOperation1.ItemLinksCount = 0;
            reduceOperation1.Operation = DevExpress.XtraBars.Ribbon.ReduceOperationType.LargeButtons;
            this.pageHethong.ReduceOperations.Add(reduceOperation1);
            this.pageHethong.Text = "Hệ Thống";
            // 
            // grpgrpTaiKhoan
            // 
            this.grpgrpTaiKhoan.ItemLinks.Add(this.btnDangXuat, true);
            this.grpgrpTaiKhoan.ItemLinks.Add(this.btnDoiMatKhau, true);
            this.grpgrpTaiKhoan.Name = "grpgrpTaiKhoan";
            this.grpgrpTaiKhoan.Text = "Tài Khoản";
            // 
            // grpSaoLuu
            // 
            this.grpSaoLuu.ItemLinks.Add(this.btnBackup, true);
            this.grpSaoLuu.ItemLinks.Add(this.btnRestore, true);
            this.grpSaoLuu.Name = "grpSaoLuu";
            this.grpSaoLuu.Text = "Sao Lưu";
            // 
            // grpQuanLy
            // 
            this.grpQuanLy.ItemLinks.Add(this.btnPhanQuyen, true);
            this.grpQuanLy.ItemLinks.Add(this.btnQuanLyNV, true);
            this.grpQuanLy.Name = "grpQuanLy";
            this.grpQuanLy.Text = "Quản Lý";
            // 
            // pageSanPham
            // 
            this.pageSanPham.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageSanPham.Appearance.Options.UseFont = true;
            this.pageSanPham.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.grpQuanLySanPham});
            this.pageSanPham.Image = global::QLShop.Properties.Resources.product_Ty9_icon;
            this.pageSanPham.Name = "pageSanPham";
            this.pageSanPham.Text = "Sản Phẩm";
            // 
            // grpQuanLySanPham
            // 
            this.grpQuanLySanPham.ItemLinks.Add(this.btnDanhMuc, true);
            this.grpQuanLySanPham.ItemLinks.Add(this.btnThietLapGia, true);
            this.grpQuanLySanPham.ItemLinks.Add(this.btnKiemKho, true);
            this.grpQuanLySanPham.Name = "grpQuanLySanPham";
            this.grpQuanLySanPham.Text = "Quản Lý Sản Phẩm";
            // 
            // pageGiaoDich
            // 
            this.pageGiaoDich.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageGiaoDich.Appearance.Options.UseFont = true;
            this.pageGiaoDich.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.grpGiaoDich});
            this.pageGiaoDich.Image = global::QLShop.Properties.Resources.trade_9Hq_icon;
            this.pageGiaoDich.Name = "pageGiaoDich";
            this.pageGiaoDich.Text = "Giao Dịch";
            // 
            // grpGiaoDich
            // 
            this.grpGiaoDich.ItemLinks.Add(this.btnNcc, true);
            this.grpGiaoDich.ItemLinks.Add(this.btnKhachHang, true);
            this.grpGiaoDich.ItemLinks.Add(this.btnTongQuan, true);
            this.grpGiaoDich.Name = "grpGiaoDich";
            this.grpGiaoDich.Text = "Giao Dịch";
            // 
            // pageTroGiup
            // 
            this.pageTroGiup.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageTroGiup.Appearance.Options.UseFont = true;
            this.pageTroGiup.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.grpTroGiup});
            this.pageTroGiup.Image = global::QLShop.Properties.Resources.helper_zjO_icon;
            this.pageTroGiup.Name = "pageTroGiup";
            this.pageTroGiup.Text = "Trợ Giúp";
            // 
            // grpTroGiup
            // 
            this.grpTroGiup.ItemLinks.Add(this.btnTimSP, true);
            this.grpTroGiup.ItemLinks.Add(this.btnTimNV, true);
            this.grpTroGiup.ItemLinks.Add(this.btnThongTin, true);
            this.grpTroGiup.Name = "grpTroGiup";
            this.grpTroGiup.Text = "Trợ Giúp";
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.lblTime);
            this.ribbonStatusBar.ItemLinks.Add(this.cboChiNhanh);
            this.ribbonStatusBar.ItemLinks.Add(this.lblShowTime);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 418);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(935, 31);
            // 
            // MainManager
            // 
            this.MainManager.MdiParent = this;
            this.MainManager.MenuManager = this.ribbon;
            this.MainManager.View = this.tabbedView1;
            this.MainManager.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // tabbedView1
            // 
            this.tabbedView1.AppearancePage.Header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tabbedView1.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabbedView1.AppearancePage.Header.ForeColor = System.Drawing.Color.Black;
            this.tabbedView1.AppearancePage.Header.Options.UseBackColor = true;
            this.tabbedView1.AppearancePage.Header.Options.UseFont = true;
            this.tabbedView1.AppearancePage.Header.Options.UseForeColor = true;
            // 
            // Time
            // 
            this.Time.Interval = 1000;
            this.Time.Tick += new System.EventHandler(this.Time_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Stretch;
            this.BackgroundImageStore = global::QLShop.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(935, 449);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "frmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage pageHethong;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup grpgrpTaiKhoan;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem btnDangXuat;
        private DevExpress.XtraBars.BarButtonItem btnDoiMatKhau;
        private DevExpress.XtraBars.BarButtonItem btnBackup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup grpSaoLuu;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup grpQuanLy;
        private DevExpress.XtraBars.Ribbon.RibbonPage pageSanPham;
        private DevExpress.XtraBars.Ribbon.RibbonPage pageGiaoDich;
        private DevExpress.XtraBars.Ribbon.RibbonPage pageTroGiup;
        private DevExpress.XtraBars.BarButtonItem btnRestore;
        private DevExpress.XtraBars.BarButtonItem btnPhanQuyen;
        private DevExpress.XtraBars.BarButtonItem btnQuanLyNV;
        private DevExpress.XtraBars.BarStaticItem lblChaoMung;
        private DevExpress.XtraBars.BarStaticItem lblTime;
        private DevExpress.XtraBars.Docking2010.DocumentManager MainManager;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.BarEditItem cboChiNhanh;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraBars.BarButtonItem btnDanhMuc;
        private DevExpress.XtraBars.BarButtonItem btnThietLapGia;
        private DevExpress.XtraBars.BarButtonItem btnKiemKho;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup grpQuanLySanPham;
        private DevExpress.XtraBars.BarButtonItem btnNcc;
        private DevExpress.XtraBars.BarButtonItem btnKhachHang;
        private DevExpress.XtraBars.BarButtonItem btnTongQuan;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup grpGiaoDich;
        private DevExpress.XtraBars.BarButtonItem btnTimSP;
        private DevExpress.XtraBars.BarButtonItem btnTimNV;
        private DevExpress.XtraBars.BarButtonItem btnThongTin;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup grpTroGiup;
        private DevExpress.XtraBars.BarStaticItem lblShowTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private System.Windows.Forms.Timer Time;
    }
}