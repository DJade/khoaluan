﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
namespace DBReference
{
    public static class DataUtilityReader
    {
        public static string connStr_Master;
        public static string connStr_DB;

        public static void InitConnection(string ConnStr_Master, string Connstr_DB = null)
        {
            connStr_DB = Connstr_DB;
            connStr_Master = ConnStr_Master;
        }

        public static DataSet GetData(string connectionString, string query)
        {
            DataSet data = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(connectionString, query);
            adapter.Fill(data);
            if(data == null)
            {
                return null;
            }
            else
            {
                return data;
            }
        }

        public static object GetScalarValue(string connectionString, string query)
        {
            DataTable data = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(connectionString, query);
            adapter.Fill(data);
            if (data == null)
            {
                return null;
            }
            else
            {
                return data.Rows[0].ItemArray[0];
            }
        }

    }
}
