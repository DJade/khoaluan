﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using DBReference;

namespace Controls
{
    public partial class DBConfiguration : UserControl
    {
        public DBConfiguration()
        {
            InitializeComponent();
            string conn = ConfigurationManager.ConnectionStrings["masterDBConnectionString"].ToString();
            DataUtilityReader.InitConnection(conn);
            rdbWin.Checked = true;
        }

        private void DBConfiguration_Load(object sender, EventArgs e)
        {            
            string query = string.Format("SELECT @@SERVERNAME");
            var data = DataUtilityReader.GetScalarValue(DataUtilityReader.connStr_Master, query);
            cboServer.Properties.Items.Add(data);
        }

        private void comboBoxEdit1_Properties_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = string.Format("SELECT name FROM sys.databases");
            var data = DataUtilityReader.GetData(DataUtilityReader.connStr_Master, query);
            foreach (DataRow row in data.Rows)
            {
                cboDatabase.Properties.Items.Add(row["name"].ToString());
            }
        }

        private void rdbWin_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbWin.Checked)
            {
                txtUserName.Enabled = txtPass.Enabled = false;
            }
            if(rdbSql.Checked)
            {
                txtUserName.Enabled = txtPass.Enabled = true;
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.ParentForm.Close();
        }

        private void btnKetNoi_Click(object sender, EventArgs e)
        {
            string conn = GeneConnectionString();
            if (string.IsNullOrEmpty(conn))
            {
                ErrorShow();
            }
            else
            {
                string query = string.Format("EXEC sp_update_dbconfiguration '{0}'", conn);
                DataUtilityReader.GetData(DataUtilityReader.connStr_Master, query);
                MessageBox.Show("Điều chỉnh kết nối thành công!");
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            string conn = GeneConnectionString();
            if (string.IsNullOrEmpty(conn))
            {
                ErrorShow();
            }
            else
            {
                string query = string.Format("SELECT 1");
                DataUtilityReader.GetData(DataUtilityReader.connStr_Master, query);
                MessageBox.Show("Kết nối thành công!");
            }
        }

        private string GeneConnectionString()
        {
            string connStr_DB = string.Empty;
            if (rdbSql.Checked && (string.IsNullOrEmpty(txtUserName.Text) || string.IsNullOrEmpty(txtPass.Text)))
            {
                return null;
            }
            if (!txtUserName.Enabled)
            {
                connStr_DB = string.Format("Data Source={0};Initial Catalog={1};Integrated Security=SSPI;", cboServer.Text, cboDatabase.Text);
            }
            else
            {
                connStr_DB = string.Format("Data Source={0};Initial Catalog={1};User Id={2};Password={3};", cboServer.Text, cboDatabase.Text, txtUserName.Text, txtPass.Text);
            }
            return connStr_DB;
        }

        private void ErrorShow()
        {
            MessageBox.Show("Thông tin đăng nhập database chưa chính xác. Không thể kết nối tới database.");
            txtUserName.Text = txtPass.Text = string.Empty;
            txtUserName.Focus();
        }
    }
}
